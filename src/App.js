import './App.css';
import Home from './views/Home'
import Navigation from './views/Navigation'
import About from './views/About'
import Article from './views/Article'
import Archive from './views/Archive'
import Category from './views/Category'
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom'
function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/" component={Home}></Route>
          <Route path="/navigation" component={Navigation}></Route>
          <Route path="/about" component={About}></Route>
          <Route path="/articles/:id" component={Article}></Route>
          <Route path="/archive" component={Archive}></Route>
          <Route path="/category" component={Category}></Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;

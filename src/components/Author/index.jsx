import React from 'react'
import { GithubOutlined, WechatOutlined, QqOutlined} from '@ant-design/icons'
import author from './author.module.css'

function Author({avatar, name}) {
  return (
    <div className={author.author}>
      <div className={author.avatar}>
        <img src="https://i2.hdslb.com/bfs/face/1541c2ea908c30fc2f33258cd513e31b8527beed.jpg@128w_128h_1o.webp" alt={name}/>
        <span className={author.name}>李梧歌</span>
        <span>想成为程序员</span>
      </div>
      <div className={author.divider}>
        <span>社交帐号</span>
      </div>
      <div className={author.contact}>
        <a href="https://www.baidu.com">
          <GithubOutlined className={author.linkIcon}/>
        </a>
        <a href="https://www.baidu.com">
          <WechatOutlined className={author.linkIcon}/>
        </a>
        <a href="https://www.baidu.com">
          <QqOutlined className={author.linkIcon}/>
        </a>
      </div>
    </div>
  )
}

export default Author

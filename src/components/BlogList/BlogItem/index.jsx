import styles from './BlogItem.module.css'
import { useHistory } from 'react-router-dom'
import useFetchFile from '../../../hooks/useFetchFile'
import coverPic from '../../../assets/images/cover.jpg'

function BlogItem({ id, title, thumbnail, content, updateTime }) {
  const history = useHistory()
  const navigateToArticleDetail = (id) => {
    history.push("/articles/" + id)
  }
  const { isPending, data, error } = useFetchFile(`http://localhost:3003/upload/${thumbnail}`)
  return (
    <div className={styles.blog}>
      {/* 头图 */}
      <div className={styles.thumbnail}>
        { (isPending || error) && <img src={coverPic} alt={title}/>}
        { data && <img src={data} alt={title}/> }
      </div>
      {/* 主要内容 */}
      <div className={styles.content}>
        <div className={styles.title} onClick={ () => navigateToArticleDetail(id) }>{ title }</div>
        <div className={styles.updateTime}>{ (new Date(updateTime)).toLocaleDateString() }</div>
        {/* 简要信息 */}
        <div className={styles.text}>{ content }</div>
      </div>
    </div>
  )
}

export default BlogItem

import React from 'react'
import BlogItem from './BlogItem'
import styles from './BlogList.module.css'

function BlogList({ blogs }) {
  
  return (
    <div className={styles.blogList}>
      {
        blogs.map(blog => {
          return <BlogItem key={blog.id} {...blog} />
        })
      }
    </div>
  )
}

export default BlogList

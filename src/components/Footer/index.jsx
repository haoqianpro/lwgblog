import styles from './Footer.module.css'

function Footer() {
  return (
    <div className={styles.footer}>
      <span>李梧歌</span>
      <span className={styles.small}>成为更好的前端开发者</span>
    </div>
  )
}

export default Footer

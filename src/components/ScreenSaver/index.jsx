import styles from './ScreenSaver.module.css'
import Header from '../../components/Header'
import { BsListUl } from 'react-icons/bs'
import { useHistory } from 'react-router-dom'

const ScreenSaver = () => {
  const history = useHistory()
  const navigateToNavPage = () => {
    history.push("/navigation")
  }
  const date = new Date()
  const today = `${date.getFullYear()}年 ${date.getMonth()+1}月 ${date.getDate()}日`
  return (
    <div className={styles.saver}>
      <Header
        subTitle="我要成为程序员"
        handleClick={() => navigateToNavPage()}
        icon={ <BsListUl /> }
      />
      <div className={styles.mask}></div>
      <div className={styles.desc}>
        <div className={styles.date}>{ today }</div>
        <div className={styles.slogan}>犯大吴疆土者，盛必击而破之。</div>
      </div>
    </div>
  )
}

export default ScreenSaver

import { IconContext } from 'react-icons'

import styles from './Header.module.css'

function Header({ subTitle, handleClick, icon, color }) {
  const iconColor = color ? color:"#fff"
  return (
    <div className={styles.header} style={{ color }}>
      <div className={styles.brand}>
        <h1 className={styles.logo}>李梧歌</h1>
        <span>{ subTitle }</span>
      </div>
      <IconContext.Provider
        value={{ color: iconColor, size: "2rem" }}
      >
        <div onClick={ () => handleClick() } className={styles.menuBar}>
          { icon }
        </div>
      </IconContext.Provider>
    </div>
  )
}

export default Header

import { IconContext } from 'react-icons'
import { BsHouse } from 'react-icons/bs'
import ReactMarkdown from 'react-markdown'
import { useParams, useHistory } from 'react-router-dom'
import styles from './Article.module.css'
import useFetch from '../../hooks/useFetch'
import Loading from '../../components/Loading'
function Article() {
  const { id } = useParams()
  const history = useHistory()
  const returnHome = () => {
    history.push("/")
  }
  const { isPending, error, data } = useFetch(`http://localhost:3003/articles/${id}`)
  return (
    <div className={styles.article}>
      {/* 页头 */}
      <div className={styles.header}>
        <IconContext.Provider value={{ color: "#666", size: "2rem"}}>
          <div style={{ cursor: "pointer"}} onClick={ () => returnHome() }>
            <BsHouse />
          </div>
        </IconContext.Provider>
      </div>
      {/* 文章内容 */}
      { isPending && <Loading /> }
      { error && {error}}
      { data && (
        <div className={styles.content}>
          <h1>{data.title}</h1>
          <div className={styles.category}>分类：{data.category}</div>
          <ReactMarkdown escapeHtml={false}>{data.content}</ReactMarkdown>
          <div className={styles.meta}>
            <div>发布于： { (new Date(data.createTime)).toLocaleDateString() }</div>
            <div>最近修改于： { (new Date(data.updateTime)).toLocaleDateString() }</div>
          </div>
        </div>
      )}
    </div>
  )
}

export default Article

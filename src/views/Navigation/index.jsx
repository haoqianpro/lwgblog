import styles from './Navigation.module.css'
import { Link, useHistory } from 'react-router-dom'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import { BsX } from 'react-icons/bs'

const Navigation = () => {
  const history = useHistory()
  const returnHome = () => {
    history.push("/")
  }
  return (
    <div className={styles.navigation}>
      <Header
        handleClick = { () => returnHome() }
        icon={ <BsX /> }
        color="#666"
      />
      <nav className={styles.nav}>
        <Link to="/category">分类</Link>
        <Link to="/archive">归档</Link>
        <Link to="/about">关于我</Link>
      </nav>
      <Footer />
    </div>
  )
}

export default Navigation
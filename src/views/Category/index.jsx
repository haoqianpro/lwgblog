import styles from './Category.module.css'
import { useState, useEffect } from 'react'
import { IconContext } from 'react-icons'
import { BsArrowLeft } from 'react-icons/bs'
import { useHistory } from 'react-router-dom'
const Category = () => {
  const history = useHistory()
  const returnToNavigate = () => {
    history.push("/navigation")
  }
  const [ data, setData ] = useState([])
  const getData = async (url) => {
    try {
      const response = await fetch(url)
      const result = await response.json()
      const set = new Set()
      result.forEach(item => {
        set.add(item.category)
      })
      setData(Array.from(set))
    } catch (error) {

    }
  }
  useEffect(() => {
    getData("http://localhost:3003/articles")
  }, [])
  return (
    <div className={styles.category}>
      <div className={styles.header}>
        <IconContext.Provider value={{ color: "#666", size: "2rem"}}>
          <div style={{ cursor: "pointer"}} onClick={ () => returnToNavigate()}>
            <BsArrowLeft />
          </div>
        </IconContext.Provider>
      </div>
      <div className={styles.inner}>
        <h2 className={styles.title}>所有分类</h2>
        {
          data && data.map((item, index) => {
            return <div key={index} className={styles.category}>{item}</div>
          })
        }
      </div>
    </div>
  )
}

export default Category
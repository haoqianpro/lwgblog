import styles from './About.module.css'
import { IconContext } from 'react-icons'
import { BsArrowLeft } from 'react-icons/bs'
import { useHistory } from 'react-router-dom'

const About = () => {
  const history = useHistory()
  const returnToNavigate = () => {
    history.push("/navigation")
  }
  return (
    <div className={styles.about}>
      <div className={styles.header}>
        <IconContext.Provider value={{ color: "#666", size: "2rem"}}>
          <div style={{ cursor: "pointer"}} onClick={ () => returnToNavigate()}>
            <BsArrowLeft />
          </div>
        </IconContext.Provider>
      </div>
      <div className={styles.inner}>
        <h1>关于我</h1>
        <p>我的名字叫吉良吉影，33岁。住在杜王町东北部的别墅区一带，未婚。我在龟友连锁店服务。每天都要加班到晚上8点才能回家。我不抽烟，酒仅止于浅尝。晚上11点睡，每天要睡足8个小时。睡前，我一定喝一杯温牛奶，然后做20分钟的柔软操，上了床，马上熟睡。一觉到天亮，决不把疲劳和压力，留到第二天。医生都说我很正常。</p>
        <p>我只是要说，我这个人别无奢求，只希望能够『心情很平静』地活下去。『胜负』、『输赢』，是我最不喜欢和人计较的。因为，那只会为自己弄来『麻烦』和『敌人』……我就是这么知足的人，这也是我的人生观。若一定要动手的话，我是不会输给任何人的。也就是说，如果出现了妨碍我睡眠的『麻烦』和『敌人』，在对方开口前……就会碰到这个！</p>
        <h2>我的优点</h2>
        <ul>
          <li>准时</li>
          <li>不喜欢说谎</li>
          <li>不承诺自己做不到的事</li>
          <li>拥有自己的底线</li>
        </ul>
      </div>
    </div>
  )
}

export default About